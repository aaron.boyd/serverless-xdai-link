var assert = require('assert');
var link = require('../createLink');

describe('createLink', () => {

  describe('handler()', () => {
    
    it('should return http status 400 when no valid body supplied', () => {

        const invalidEvent = {
            resource: "/",
            path: "/link",
            httpMethod: "POST"
        };
        const expectedBody = JSON.stringify({message:"Bad request - No reward information passed"});
        link.handler(invalidEvent, null, (err, response) => {            
            assert.equal(response.statusCode, 400);            
            assert.equal(response.body, expectedBody);
        });
    });

    it('should return 500 http status (due to unfunded account) otherwise', () => {

        const validEvent = {
            resource: "/",
            path: "/link",
            httpMethod: "POST",
            body: "{\"rewardInXDAI\": \".000000000000000001\"}"
        };
        const expectedMessagePart = "Server error during creation of xDAI reward link";
        link.handler(validEvent, null, (err, response) => {            
            assert.equal(response.statusCode, 500);
            console.log(response.body);
            const resultBody = JSON.parse(response.body);
            const contains = resultBody.message.indexOf(expectedMessagePart,0) >= 0;
            assert(contains);            
        });
    });
  });
});

