// Adapted to be standalone programmatic version of Burner Wallet LinksPlugin
// From https://github.com/dmihal/burner-wallet/blob/burner-2-prototype/plugins/src/link/LinkPlugin.ts

const Web3 = require('web3');
const linkAbi = require('../abis/Links.json');
const utils = require('./utils');
const Transaction = require('ethereumjs-tx').Transaction;
const Common = require('ethereumjs-common');

const LINK_XDAI_CONTRACT_ADDRESS = process.env.LINK_XDAI_CONTRACT_ADDRESS;
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';
const RELAY_GAS_PRICE = process.env.RELAY_GAS_PRICE;
const GAS_LIMIT = process.env.GAS_LIMIT;
const BURNER_WALLET_HOST = process.env.BURNER_WALLET_HOST;
const XDAI_NETWORK = {
    name: 'xdai',
    networkId: 100,
    chainId: 100,
};

getWeb3 = async () => {
    const awsSecret = await utils.getSecret('xDAIweb3HttpsProvider');
    const jsonSecret = JSON.parse(awsSecret);
    return new Web3(new Web3.providers.HttpProvider(jsonSecret.provider));
}

getContract = (web3) => {
    return new web3.eth.Contract(linkAbi, LINK_XDAI_CONTRACT_ADDRESS);;
}

// Returns the link format required for https://xdai.io
const getClaimUrl = (claimId, claimKey) => {
    return `${BURNER_WALLET_HOST}/${claimId};${claimKey}`;
};

createLink = async (xdai) => {

    console.log(`### 0 ### Starting link creation for ${xdai} xDAI`);

    const web3 = await getWeb3();
    const linkContract = getContract(web3);

    // 1 Create random hash and new account    
    const randomHash = web3.utils.sha3(Math.random().toString());
    const randomWallet = web3.eth.accounts.create();
    console.log(`### 1 ### Created random hash (${randomHash}) and new account ${randomWallet.address}`);

    // 2 Sign random hash with new account   
    const sig = web3.eth.accounts.sign(randomHash, randomWallet.privateKey);
    console.log(`### 2 ### Signed random hash with new account ${randomWallet.address}`);

    // 3 Create transaction with source funding account to the Links contract    
    const linkExpirationTime = 2; // Hard-coded to 2 days link expiration.
    const weiToSend = web3.utils.toWei(xdai.toString(), 'ether');

    // Retrieve privateKey and address from managed secret
    const awsSecret = await utils.getSecret('xDAISigner');
    const jsonSecret = JSON.parse(awsSecret);
    const sourceAccountPrivateKey = new Buffer.from(jsonSecret.privateKey, 'hex');
    const sourceAccountNonce = await web3.eth.getTransactionCount(jsonSecret.address);

    // Create transaction
    const transactionData = linkContract.methods.send(randomHash, sig.signature, ZERO_ADDRESS, weiToSend, linkExpirationTime).encodeABI();
    // For xDAI, all of these network's params are the same than mainnets', except for name, chainId, and networkId, so we use the Common.forCustomChain method
    const customCommon = Common.default.forCustomChain('mainnet', XDAI_NETWORK, 'petersburg');
    const rawTransaction = {
        nonce: web3.utils.toHex(sourceAccountNonce),
        gasPrice: web3.utils.toHex(RELAY_GAS_PRICE),
        gasLimit: web3.utils.toHex(GAS_LIMIT),
        to: LINK_XDAI_CONTRACT_ADDRESS,
        value: web3.utils.toHex(weiToSend),
        data: transactionData,
        chainId: 100
    }
    console.log(`### 3 ### Created raw transaction ${JSON.stringify(rawTransaction)}`);

    // 4 Sign and send transaction
    const transaction = new Transaction(rawTransaction, {
        common: customCommon
    });
    transaction.sign(sourceAccountPrivateKey);
    const serializedTx = transaction.serialize();
    const receipt = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
    console.log(`### 4 ### Sent signed transaction, receipt: ${JSON.stringify(receipt)}`);

    // 5 Construct claim URL
    const claimUrl = getClaimUrl(randomHash, randomWallet.privateKey);
    console.log(`### 5 ### Created claim link `);

    return {
        claimUrl,
        receipt
    };
}

module.exports.createXDAILink = async (xdaiObj) => {
    
    const xDAIToLink = xdaiObj.rewardInXDAI;
    
    if(parseFloat(xDAIToLink)>0){
        const { claimUrl, receipt } = await createLink(xDAIToLink);
        console.log(`Link created. Generated claimUrl = ${claimUrl}`);
        xdaiObj["claimUrl"] = claimUrl;
        xdaiObj["receipt"] = receipt;
    }
    else{
        xdaiObj["hashCode"] = getWeb3().utils.sha3(Math.random().toString()); // give no link a random hashCode anyway
        console.log('Link not created. Cannot send 0');
    }
    xdaiObj["createdTimestamp"] = utils.getTimestamp();
    return xdaiObj;
}